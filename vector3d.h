#ifndef VECTOR3D_H
#define VECTOR3D_H

class vector3d
{

public:
    vector3d();
    vector3d(float x, float y, float z);
    vector3d(const vector3d &vec);

    vector3d &operator=(const vector3d &vec);

    void set(float x, float y, float z);

    float x, y, z;
};

#endif // VECTOR3D_H
