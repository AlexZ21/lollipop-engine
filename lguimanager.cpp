#include "lguimanager.h"

LGUIManager::LGUIManager()
{
}

LGUI *LGUIManager::createGui()
{
    LGUI *gui = NULL;
    gui = new LGUI();
    if(gui)
    {
        guiList.push_back(gui);
        Log::print()<<"Create gui";
    }else{
        Log::print()<<"Error create gui";
        return NULL;
    }
}

void LGUIManager::addGui(LGUI *gui)
{
    if(gui)
    {
        Log::print()<<"Add gui";
        guiList.push_back(gui);
    }else{
        Log::print()<<"Erorr add gui";
    }
}

void LGUIManager::removeGui(LGUI *gui)
{
    if(gui)
    {
        guiList.remove(gui);
        delete gui;
        Log::print()<<"Remove gui";
    }else{
        Log::print()<<"Error remove gui";
    }
}

void LGUIManager::renoveAllGui()
{
    if(!guiList.empty())
    {
        guiList.clear();
        Log::print()<<"Remove all gui";
    }else{
        Log::print()<<"Gui list is empty";
    }
}
