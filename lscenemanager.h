#ifndef LSCENEMANAGER_H
#define LSCENEMANAGER_H

#include "log.h"
#include "lscene.h"
#include <list>

class LSceneManager
{
    std::list<LScene *> sceneList;
public:
    LSceneManager();

    LScene *createScene();
    void addScene(LScene *scene);
    void removeScene(LScene *scene);
    void removeAllScene();

    void update();
};

#endif // LSCENEMANAGER_H
