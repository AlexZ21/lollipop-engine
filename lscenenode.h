#ifndef LSCENENODE_H
#define LSCENENODE_H

#include "vector3d.h"


class LSceneNode
{
public:
    LSceneNode();
    virtual void draw() = 0;
};

#endif // LSCENENODE_H
