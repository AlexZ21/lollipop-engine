#ifndef LGUIMANAGER_H
#define LGUIMANAGER_H

#include "lgui.h"
#include "log.h"

#include <list>

class LGUIManager
{   
    std::list<LGUI *> guiList;
public:
    LGUIManager();

    LGUI *createGui();
    void addGui(LGUI *gui);
    void removeGui(LGUI *gui);
    void renoveAllGui();
};

#endif // LGUIMANAGER_H
