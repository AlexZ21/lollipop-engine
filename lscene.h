#ifndef LSCENE_H
#define LSCENE_H

#include "lscenecamera.h"
#include "lscenenodegroup.h"
#include "lscenenode.h"
#include "lscenesimplecube.h"
#include "log.h"
#include "lguimanager.h"

#include <list>

class LScene
{
    bool state;
    bool visible;

    std::list<LSceneNodeGroup *> sceneNodeGroupList;
    LSceneNodeGroup *rootSceneNodeGroup;

    LGUIManager *guiManager;
    LSceneCamera *sceneCamera;

public:
    LScene();
    ~LScene();

    //Scene node group
    LSceneNodeGroup *createSceneNodeGroup();
    void addSceneNodeGroup(LSceneNodeGroup *sceneNodeGroup);
    void removeSceneNodeGroup(LSceneNodeGroup *sceneNodeGroup);
    LSceneNodeGroup *getRootSceneNodeGroup();


    //Scene node
    LSceneNode *createSceneNode();
    void addSceneNode(LSceneNode *sceneNode);
    void removeSceneNode(LSceneNode *sceneNode);
    void renoveAllSceneNode();


    //GUI
    LGUI *createGUI();
    void addGUI(LGUI *gui);
    void removeGUI(LGUI *gui);
    void renoveAllGUI();


    //Scene camera
    LSceneCamera *createSceneCamera();


    void setActive(bool state);
    bool isActive();

    void setVisible(bool visible);
    bool isVisible();

    void draw();

};

#endif // LSCENE_H
