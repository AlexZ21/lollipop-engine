#include <iostream>
#include "log.h"
#include "ldevice.h"

using namespace std;

int main()
{
    Log::startLog("log2.txt", true);
    Log::print()<<"Погнали посоны!!";
    Log::print()<<"Движок рвет!!";

    LollipopDevice device;

    LWindow *window = device.createWindow(800, 600, 24, false, true, true);
    window->setWindowTitle("Шикарная прога");

    LEvent *levent = window->getEventHandler();

    LSceneManager *sceneManager = device.getSceneManager();

    LScene *scene = sceneManager->createScene();
    scene->setActive(true);

    //LSceneNode *node = scene->createSceneNode();
    LSceneSimpleCube *node = new LSceneSimpleCube(scene->getRootSceneNodeGroup());
    node->setRotation(vector3d(0,0,0));
    node->setPosition(vector3d(-10,0,0));

    LSceneNodeGroup *newGroup = new LSceneNodeGroup();
    scene->addSceneNodeGroup(newGroup);
    newGroup->setVisible(true);

    LSceneSimpleCube *node2 = new LSceneSimpleCube(newGroup);
    node2->setRotation(vector3d(0,0,0));
    node2->setPosition(vector3d(10,0,0));

    LSceneCamera *camera = scene->createSceneCamera();
    camera->setPosition(vector3d(0.0f, 0.4f, 30.0f));

    LGUI *gui = scene->createGUI();

    int rot = 0;

    while(scene->isActive())
    {
        rot+=1;

        node->setRotation(vector3d(rot, rot, rot));
        node2->setRotation(vector3d(-rot, rot, -rot));

        if(levent->mouseHit(LBUTTON_LEFT))
            window->setFullscreen(true);

        if(levent->mousePress(LBUTTON_RIGHT))
            device.exit();

        if(levent->keyHit(LKEY_SPACE))
            window->setFullscreen(false);

        if(levent->keyHit(LKEY_A))
            window->setFullscreen(true);

        if(levent->keyHit(LKEY_ENTER))
            scene->setActive(false);

        device.update();

    }

    sceneManager->removeAllScene();

    device.exit();
    Log::closeLog();


    return 0;
}

