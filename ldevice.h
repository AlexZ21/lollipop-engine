#ifndef LENGINE_H
#define LENGINE_H

#include "lwindow.h"
#include "levent.h"
#include "lscenemanager.h"

#ifdef _WIN32
   //define something for Windows
#elif __linux__
  #include "x11/x11window.h"
#endif


class LollipopDevice
{

    LWindow *window;
    LSceneManager *sceneManager;
public:
    LollipopDevice();
    ~LollipopDevice();

    LWindow *createWindow(unsigned int width,unsigned int height,unsigned int depth, bool fullscreen, bool vsync, bool doubleBuffered);
    LSceneManager *getSceneManager();


    void update();
    void exit();
};

#endif // LENGINE_H
