#ifndef LSCENECAMERA_H
#define LSCENECAMERA_H

#include "vector3d.h"
#include "log.h"

#include <GL/gl.h>
#include <GL/glx.h>
#include <GL/glu.h>

class LSceneCamera
{
    vector3d position;
    vector3d rotation;
    vector3d vector;
public:
    LSceneCamera();
    void setPosition(vector3d position);
    void setRotation(vector3d rotation);
    void setVector(vector3d vector);

    void update();
};

#endif // LSCENECAMERA_H
