TEMPLATE = app
CONFIG += console
CONFIG -= app_bundle
CONFIG -= qt

LIBS += -lGL -lGLU -lX11 -lXxf86vm

SOURCES += main.cpp \
    x11/x11window.cpp \
    log.cpp \
    lwindow.cpp \
    ldevice.cpp \
    levent.cpp \
    x11/x11event.cpp \
    x11/x11platform.cpp \
    lscenemanager.cpp \
    lscene.cpp \
    vector3d.cpp \
    lscenenode.cpp \
    lguimanager.cpp \
    lgui.cpp \
    lscenecamera.cpp \
    lscenesimplecube.cpp \
    lscenenodegroup.cpp

HEADERS += \
    x11/x11window.h \
    log.h \
    lwindow.h \
    ldevice.h \
    levent.h \
    x11/x11event.h \
    x11/x11platform.h \
    lscenemanager.h \
    lscene.h \
    vector3d.h \
    lscenenode.h \
    lguimanager.h \
    lgui.h \
    lscenecamera.h \
    lscenesimplecube.h \
    lscenenodegroup.h

