#include "lscenecamera.h"

LSceneCamera::LSceneCamera()
{
}

void LSceneCamera::setPosition(vector3d position)
{
    this->position = position;
}

void LSceneCamera::setRotation(vector3d rotation)
{
    this->rotation = rotation;
}

void LSceneCamera::setVector(vector3d vector)
{
    this->vector = vector;
}

void LSceneCamera::update()
{
    glMatrixMode( GL_MODELVIEW );
    glLoadIdentity();

    glRotated(rotation.z, 0.0, 0.0, 1.0);
    glRotated(rotation.y, 0.0, 1.0, 0.0);
    glRotated(rotation.x, 1.0, 0.0, 0.0);

    glTranslated(-position.x, -position.y, -position.z);
}
