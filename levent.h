#ifndef LEVENT_H
#define LEVENT_H

enum LKey{

    LKEY_UNKNOWN = 0,

    LKEY_SPACE = 1,

    LKEY_APOSTROPHE = 2, /* ' */

    LKEY_COMMA = 3, /* , */

    LKEY_MINUS = 4, /* - */

    LKEY_PERIOD = 5, /* . */

    LKEY_SLASH = 6, /* / */

    LKEY_0 = 7,
    LKEY_1 = 8,
    LKEY_2 = 9,
    LKEY_3 = 10,
    LKEY_4 = 11,
    LKEY_5 = 12,
    LKEY_6 = 13,
    LKEY_7 = 14,
    LKEY_8 = 15,
    LKEY_9 = 16,

    LKEY_SEMICOLON = 17, /* ; */

    LKEY_EQUAL = 18, /* = */

    LKEY_A = 19,
    LKEY_B = 20,
    LKEY_C = 21,
    LKEY_D = 22,
    LKEY_E = 23,
    LKEY_F = 24,
    LKEY_G = 25,
    LKEY_H = 26,
    LKEY_I = 27,
    LKEY_J = 28,
    LKEY_K = 29,
    LKEY_L = 30,
    LKEY_M = 31,
    LKEY_N = 32,
    LKEY_O = 33,
    LKEY_P = 34,
    LKEY_Q = 35,
    LKEY_R = 36,
    LKEY_S = 37,
    LKEY_T = 38,
    LKEY_U = 39,
    LKEY_V = 40,
    LKEY_W = 41,
    LKEY_X = 42,
    LKEY_Y = 43,
    LKEY_Z = 44,

    LKEY_LEFT_BRACKET = 45, /* [ */

    LKEY_BACKSLASH = 46, /* \ */

    LKEY_RIGHT_BRACKET = 47, /* ] */

    LKEY_GRAVE_ACCENT = 48, /* ` */

    LKEY_WORLD_1 = 49, /* non-US #1 */

    LKEY_WORLD_2 = 50, /* non-US #2 */

    LKEY_ESCAPE = 51,

    LKEY_ENTER = 52,

    LKEY_TAB = 53,

    LKEY_BACKSPACE = 54,

    LKEY_INSERT = 55,

    LKEY_DELETE = 56,

    LKEY_RIGHT = 57,

    LKEY_LEFT = 58,

    LKEY_DOWN = 59,

    LKEY_UP = 60,

    LKEY_PAGE_UP = 61,

    LKEY_PAGE_DOWN = 62,

    LKEY_HOME = 63,

    LKEY_END = 64,

    LKEY_CAPS_LOCK = 65,

    LKEY_SCROLL_LOCK = 66,

    LKEY_NUM_LOCK = 67,

    LKEY_PRINT_SCREEN = 68,

    LKEY_PAUSE = 69,

    LKEY_F1 = 70,
    LKEY_F2 = 71,
    LKEY_F3 = 72,
    LKEY_F4 = 73,
    LKEY_F5 = 74,
    LKEY_F6 = 75,
    LKEY_F7 = 76,
    LKEY_F8 = 77,
    LKEY_F9 = 78,
    LKEY_F10 = 79,
    LKEY_F11 = 80,
    LKEY_F12 = 81,
    LKEY_F13 = 82,
    LKEY_F14 = 83,
    LKEY_F15 = 84,
    LKEY_F16 = 85,
    LKEY_F17 = 86,
    LKEY_F18 = 87,
    LKEY_F19 = 88,
    LKEY_F20 = 89,
    LKEY_F21 = 90,
    LKEY_F22 = 91,
    LKEY_F23 = 92,
    LKEY_F24 = 93,
    LKEY_F25 = 94,

    LKEY_KP_0 = 95,
    LKEY_KP_1 = 96,
    LKEY_KP_2 = 97,
    LKEY_KP_3 = 98,
    LKEY_KP_4 = 99,
    LKEY_KP_5 = 100,
    LKEY_KP_6 = 101,
    LKEY_KP_7 = 102,
    LKEY_KP_8 = 103,
    LKEY_KP_9 = 104,

    LKEY_KP_DECIMAL = 105,
    LKEY_KP_DIVIDE = 106,
    LKEY_KP_MULTIPLY = 107,
    LKEY_KP_SUBTRACT = 108,
    LKEY_KP_ADD = 109,
    LKEY_KP_ENTER = 110,
    LKEY_KP_EQUAL = 111,

    LKEY_LEFT_SHIFT = 112,

    LKEY_LEFT_CONTROL = 113,

    LKEY_LEFT_ALT = 114,

    LKEY_LEFT_SUPER = 115,

    LKEY_RIGHT_SHIFT = 116,

    LKEY_RIGHT_CONTROL = 117,

    LKEY_RIGHT_ALT = 118,

    LKEY_RIGHT_SUPER  = 119,

    LKEY_MENU = 120
};

enum LButton{
    LBUTTON_LEFT = 1,
    LBUTTON_CENTER = 2,
    LBUTTON_RIGHT = 3,
    LBUTTON_WHEEL_UP = 4,
    LBUTTON_WHEEL_DOWN = 5,
    LBUTTON_WHEEL_LEFT = 6,
    LBUTTON_WHEEL_RIGHT = 7,
    LBUTTON_8 = 8,
    LBUTTON_9 = 9
};


class LEvent
{
public:
    LEvent();

    virtual bool keyPress(int key) = 0;
    virtual bool keyHit(int key) = 0;
    virtual bool mousePress(int but) = 0;
    virtual bool mouseHit(int but) = 0;
};

#endif // LEVENT_H
