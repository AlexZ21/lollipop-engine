#ifndef X11EVENT_H
#define X11EVENT_H

#include "log.h"
#include <iostream>
#include "levent.h"
#include <X11/Xlib.h>
#include <X11/keysym.h>


#include <map>

class x11event : public LEvent
{

    bool keyStateArray[121][2];
    bool mouseStateArray[9][2];
public:
    x11event();
    ~x11event();
    int translateKey(Display *d, int keycode);
    bool keyPress(int key);
    bool keyHit(int key);

    int translateButton(Display *d, int button);
    bool mousePress(int but);
    bool mouseHit(int but);

    void update(Display *d, XEvent *event);

};

#endif // X11EVENT_H
