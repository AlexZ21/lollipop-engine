#include "x11window.h"

static int ctxErrorOccurred = false;
static int ctxErrorHandler(Display *dpy, XErrorEvent *ev)
{
    ctxErrorOccurred = true;
    return 0;
}

x11window::x11window()
{
    ctxErrorOccurred = false;
    display = NULL;
    vi = NULL;
    ev = NULL;
    context = 0;

    Log::print()<<"Вызывается конструктор x11window";

}

x11window::~x11window()
{
    destroyWindow();
    delete title;
    delete display;
    delete vi;
    delete ev;
}

void x11window::createWindow(int x, int y, unsigned int width, unsigned int height, unsigned int depth, bool fullscreen, bool vsync, bool doubleBuffered)
{

    this->x = x;
    this->y = y;
    this->width = width;
    this->height = height;
    this->depth = depth;
    this->fullscreen = fullscreen;
    this->vsync = vsync;
    this->doubleBuffered = doubleBuffered;


    int visual_attribs[] =
    {
        GLX_X_RENDERABLE    , True,
        GLX_DRAWABLE_TYPE   , GLX_WINDOW_BIT,
        GLX_RENDER_TYPE     , GLX_RGBA_BIT,
        GLX_X_VISUAL_TYPE   , GLX_TRUE_COLOR,
        GLX_RED_SIZE        , 8,
        GLX_GREEN_SIZE      , 8,
        GLX_BLUE_SIZE       , 8,
        GLX_ALPHA_SIZE      , 8,
        GLX_DEPTH_SIZE      , this->depth,
        GLX_STENCIL_SIZE    , 8,
        GLX_DOUBLEBUFFER    , this->doubleBuffered,
        //GLX_SAMPLE_BUFFERS  , 1,
        //GLX_SAMPLES         , 4,
        None
    };



    display = platform.display;
    screen = platform.screen;



    platform.getGLXVersion();



    platform.getBestVideoMode(this->width, this->height);



    GLXFBConfig bestFbc = platform.getBestGLXFBConfig(visual_attribs);




    if(bestFbc)
    {

        vi = glXGetVisualFromFBConfig(display, bestFbc);
        Log::print()<<"Chosen visual ID = 0x"<<vi->visualid;

        ctxErrorOccurred = false;
        int (*oldHandler)(Display*, XErrorEvent*) = XSetErrorHandler(&ctxErrorHandler);
        XSetErrorHandler(&ctxErrorHandler);



        if (!platform.isExtensionSupported("GLX_ARB_create_context" ) || !platform.glXCreateContextAttribsARB )
        {
            Log::print()<<"glXCreateContextAttribsARB() not found\n"
                          "... using old-style GLX context";
            context = glXCreateNewContext( display, bestFbc, GLX_RGBA_TYPE, 0, True );
        }else{
            Log::print()<<"glXCreateContextAttribsARB() found\n";
            int context_attribs[] =
            {
                GLX_CONTEXT_MAJOR_VERSION_ARB, 2,
                GLX_CONTEXT_MINOR_VERSION_ARB, 1,
                //GLX_CONTEXT_FLAGS_ARB        , GLX_CONTEXT_FORWARD_COMPATIBLE_BIT_ARB,
                None
            };

            context = platform.glXCreateContextAttribsARB(display, bestFbc, NULL, True, context_attribs);

            XSync(display, False);

            if (!ctxErrorOccurred && context)
            {
                Log::print()<<"Created GL context";
            }else{
                context_attribs[1] = 1;
                context_attribs[3] = 0;

                ctxErrorOccurred = false;

                Log::print()<<"Failed to create GL 3.0 context"
                              "... using old-style GLX context";
                context = platform.glXCreateContextAttribsARB(display, bestFbc, 0, True, context_attribs);
            }

            XSync(display, False);
            XSetErrorHandler(oldHandler);


            if ( ctxErrorOccurred || !context)
            {
                Log::print()<<"Failed to create an OpenGL context";
            }

        }
    }else{

            vi = glXChooseVisual(display, screen, visual_attribs);

            context = glXCreateContext(display, vi, 0, GL_TRUE);

    }


    cmap = XCreateColormap(display, RootWindow(display, vi->screen),vi->visual, AllocNone);

    winAttr.colormap = cmap;
    winAttr.border_pixel = 0;

    winAttr.event_mask = ExposureMask | KeyPressMask | KeyReleaseMask | ButtonPressMask | ButtonReleaseMask | StructureNotifyMask;
    window = XCreateWindow(display, RootWindow(display, vi->screen),
                           0, 0, this->width, this->height, 0, vi->depth, InputOutput, vi->visual,
                           CWBorderPixel | CWColormap | CWEventMask, &winAttr);

    wmDelete = XInternAtom(display, "WM_DELETE_WINDOW", True);
    XSetWMProtocols(display, window, &wmDelete, 1);
    XSetStandardProperties(display, window, this->title, this->title, None, NULL, 0, NULL);

    XFree(vi);



    XMapRaised(display, window);

    if (!glXIsDirect(display, context))
    {
        Log::print()<<"Indirect GLX rendering context obtained";
    }
    else
    {
        Log::print()<<"Direct GLX rendering context obtained";
    }

    Log::print()<<"Making context current";
    glXMakeCurrent(display, window, context);


    platform.getOpenGLVersion();

    if(this->vsync) platform.setSwapInterval(1);

    setFullscreen(this->fullscreen);



    initGL();
}

void x11window::setWindowTitle(const char *title)
{
    this->title = title;
    Atom Atom_name = XInternAtom(display,"_NET_WM_NAME",false);
    Atom Atom_utf_type = XInternAtom(display,"UTF8_STRING",false);

    XChangeProperty(display,window,Atom_name,Atom_utf_type,8,PropModeReplace,(unsigned char*)title,strlen(title));
}

void x11window::destroyWindow()
{
    if(this->context)
    {
        if( !glXMakeCurrent(display, None, NULL))
        {
            //Log::printMessage("Could not release drawing context.");
        }
        glXDestroyContext(display, context);
        context = NULL;
    }
    if(this->fullscreen)
    {
        this->fullscreen = 0;

        platform.setDesktopVideoMode();

        setWindowFullscreen(this->fullscreen);

    }
    XCloseDisplay(display);
}


void x11window::update()
{
    XEvent event;
    while (XPending(display) > 0)
    {
        XNextEvent(display, &event);
        switch (event.type)
        {
        case Expose:
            if (event.xexpose.count != 0)
                break;
            updateGL();
            break;
        case ClientMessage:
            if (strcmp(XGetAtomName(display, event.xclient.message_type), "WM_PROTOCOLS") == 0)
            {
                destroyWindow();
            }
            break;
        case ConfigureNotify:
            //if ((event.xconfigure.width != this->width) || (event.xconfigure.height != this->height))
        {
            currentWidth = event.xconfigure.width;
            currentHeight = event.xconfigure.height;
            resizeGL(currentWidth, currentHeight);
        }
            break;
        default:
            break;
        }
        if(ev != NULL) ev->update(display, &event);
    }

    updateGL();
}

x11event *x11window::getEventHandler()
{
    if(ev == NULL) ev = new x11event();
    return ev;
}

void x11window::setFullscreen(bool mode)
{

    if(mode == true)
    {
        platform.setFullscreenVideoMode();

        this->fullscreen = 1;

        setWindowFullscreen(this->fullscreen);

        XGrabKeyboard(display, window, True, GrabModeAsync, GrabModeAsync, CurrentTime);
        XGrabPointer(display, window, True, ButtonPressMask, GrabModeAsync, GrabModeAsync, window, None, CurrentTime);
    }
    if(mode == false){
        platform.setDesktopVideoMode();

        this->fullscreen = 0;

        setWindowFullscreen(this->fullscreen);


        XUngrabKeyboard(display,CurrentTime);
        XUngrabPointer(display, CurrentTime);
    }

}


void x11window::setWindowFullscreen(bool mode)
{
    XEvent xev;
    Atom wm_state = XInternAtom(display, "_NET_WM_STATE", False);
    Atom fullscreen = XInternAtom(display, "_NET_WM_STATE_FULLSCREEN", False);

    memset(&xev, 0, sizeof(xev));
    xev.type = ClientMessage;
    xev.xclient.window = window;
    xev.xclient.message_type = wm_state;
    xev.xclient.format = 32;
    if(mode)
        xev.xclient.data.l[0] = 1;
    else
        xev.xclient.data.l[0] = 0;
    xev.xclient.data.l[1] = fullscreen;
    xev.xclient.data.l[2] = 0;
    xev.xclient.data.l[3] = 1;

    XSendEvent(display, DefaultRootWindow(display), False, SubstructureNotifyMask, &xev);
}

void x11window::setSwapInterval(int interval)
{
    platform.setSwapInterval(interval);
}


int x11window::windowWidth()
{
    return currentWidth;
}

int x11window::windowHeight()
{
    return currentHeight;
}




//OPENGL FUNC
void x11window::initGL()
{
    glShadeModel(GL_SMOOTH);
    glClearColor(0.0f, 0.0f, 0.0f, 0.0f);
    glClearDepth(1.0f);
    glEnable(GL_DEPTH_TEST);
    glDepthFunc(GL_LEQUAL);
    glHint(GL_PERSPECTIVE_CORRECTION_HINT, GL_NICEST);
    /* we use resizeGL once to set up our initial perspective */
    resizeGL(width, height);

    glFlush();
}

void x11window::resizeGL(unsigned int w, unsigned int h)
{
    if (h == 0)
        h = 1;
    glViewport(0, 0, w, h);
}

void x11window::updateGL()
{
    if (doubleBuffered)
    {
        glXSwapBuffers(display, window);
    }
}

