#include "x11event.h"

x11event::x11event()
{
    for(int i = 0; i < 9; ++i)
        for(int j = 0; j < 2; ++j)
        mouseStateArray[i][j] = false;

    for(int i = 0; i < 121; ++i)
        for(int j = 0; j < 2; ++j)
        keyStateArray[i][j] = false;
}

x11event::~x11event()
{
    delete keyStateArray;
    delete mouseStateArray;
}

int x11event::translateKey(Display *d, int keycode)
{
    KeySym key;

    key = XKeycodeToKeysym(d, keycode, 0 );

    switch(key)
    {
    case XK_Escape:         return LKEY_ESCAPE;
    case XK_space:          return LKEY_SPACE;
    case XK_apostrophe:     return LKEY_APOSTROPHE;
    case XK_comma:          return LKEY_COMMA;
    case XK_minus:          return LKEY_MINUS;
    case XK_period:         return LKEY_PERIOD;
    case XK_slash:          return LKEY_SLASH;

    case XK_0:              return LKEY_0;
    case XK_1:              return LKEY_1;
    case XK_2:              return LKEY_2;
    case XK_3:              return LKEY_3;
    case XK_4:              return LKEY_4;
    case XK_5:              return LKEY_5;
    case XK_6:              return LKEY_6;
    case XK_7:              return LKEY_7;
    case XK_8:              return LKEY_8;
    case XK_9:              return LKEY_9;

    case XK_semicolon:      return LKEY_SEMICOLON;
    case XK_equal:          return LKEY_EQUAL;

    case XK_A:              return LKEY_A;
    case XK_B:              return LKEY_B;
    case XK_C:              return LKEY_C;
    case XK_D:              return LKEY_D;
    case XK_E:              return LKEY_E;
    case XK_F:              return LKEY_F;
    case XK_G:              return LKEY_G;
    case XK_H:              return LKEY_H;
    case XK_I:              return LKEY_I;
    case XK_J:              return LKEY_J;
    case XK_K:              return LKEY_K;
    case XK_L:              return LKEY_L;
    case XK_M:              return LKEY_M;
    case XK_N:              return LKEY_N;
    case XK_O:              return LKEY_O;
    case XK_P:              return LKEY_P;
    case XK_Q:              return LKEY_Q;
    case XK_R:              return LKEY_R;
    case XK_S:              return LKEY_S;
    case XK_T:              return LKEY_T;
    case XK_U:              return LKEY_U;
    case XK_V:              return LKEY_V;
    case XK_W:              return LKEY_W;
    case XK_X:              return LKEY_X;
    case XK_Y:              return LKEY_Y;
    case XK_Z:              return LKEY_Z;

    case XK_a:              return LKEY_A;
    case XK_b:              return LKEY_B;
    case XK_c:              return LKEY_C;
    case XK_d:              return LKEY_D;
    case XK_e:              return LKEY_E;
    case XK_f:              return LKEY_F;
    case XK_g:              return LKEY_G;
    case XK_h:              return LKEY_H;
    case XK_i:              return LKEY_I;
    case XK_j:              return LKEY_J;
    case XK_k:              return LKEY_K;
    case XK_l:              return LKEY_L;
    case XK_m:              return LKEY_M;
    case XK_n:              return LKEY_N;
    case XK_o:              return LKEY_O;
    case XK_p:              return LKEY_P;
    case XK_q:              return LKEY_Q;
    case XK_r:              return LKEY_R;
    case XK_s:              return LKEY_S;
    case XK_t:              return LKEY_T;
    case XK_u:              return LKEY_U;
    case XK_v:              return LKEY_V;
    case XK_w:              return LKEY_W;
    case XK_x:              return LKEY_X;
    case XK_y:              return LKEY_Y;
    case XK_z:              return LKEY_Z;

    case XK_bracketleft:    return LKEY_LEFT_BRACKET;
    case XK_backslash:      return LKEY_BACKSLASH;
    case XK_bracketright:   return LKEY_RIGHT_BRACKET;
    case XK_grave:          return LKEY_GRAVE_ACCENT;

    case XK_Tab:            return LKEY_TAB;
    case XK_BackSpace:      return LKEY_BACKSPACE;
    case XK_Insert:         return LKEY_INSERT;
    case XK_Delete:         return LKEY_DELETE;
    case XK_Return:         return LKEY_ENTER;

    case XK_Right:          return LKEY_RIGHT;
    case XK_Left:           return LKEY_LEFT;
    case XK_Down:           return LKEY_DOWN;
    case XK_Up:             return LKEY_UP;

    case XK_Page_Up:        return LKEY_PAGE_UP;
    case XK_Page_Down:      return LKEY_PAGE_DOWN;
    case XK_Home:           return LKEY_HOME;
    case XK_End:            return LKEY_END;

    case XK_Caps_Lock:      return LKEY_CAPS_LOCK;
    case XK_Scroll_Lock:    return LKEY_SCROLL_LOCK;
    case XK_Num_Lock:       return LKEY_NUM_LOCK;

    case XK_Pause:          return LKEY_PAUSE;

    case XK_F1:             return LKEY_F1;
    case XK_F2:             return LKEY_F2;
    case XK_F3:             return LKEY_F3;
    case XK_F4:             return LKEY_F4;
    case XK_F5:             return LKEY_F5;
    case XK_F6:             return LKEY_F6;
    case XK_F7:             return LKEY_F7;
    case XK_F8:             return LKEY_F8;
    case XK_F9:             return LKEY_F9;
    case XK_F10:            return LKEY_F10;
    case XK_F11:            return LKEY_F11;
    case XK_F12:            return LKEY_F12;
    case XK_F13:            return LKEY_F13;
    case XK_F14:            return LKEY_F14;
    case XK_F15:            return LKEY_F15;
    case XK_F16:            return LKEY_F16;
    case XK_F17:            return LKEY_F17;
    case XK_F18:            return LKEY_F18;
    case XK_F19:            return LKEY_F19;
    case XK_F20:            return LKEY_F20;
    case XK_F21:            return LKEY_F21;
    case XK_F22:            return LKEY_F22;
    case XK_F23:            return LKEY_F23;
    case XK_F24:            return LKEY_F24;
    case XK_F25:            return LKEY_F25;

    case XK_KP_0:         return LKEY_KP_0;
    case XK_KP_1:         return LKEY_KP_1;
    case XK_KP_2:         return LKEY_KP_2;
    case XK_KP_3:         return LKEY_KP_3;
    case XK_KP_4:         return LKEY_KP_4;
    case XK_KP_5:         return LKEY_KP_5;
    case XK_KP_6:         return LKEY_KP_6;
    case XK_KP_7:         return LKEY_KP_7;
    case XK_KP_8:         return LKEY_KP_8;
    case XK_KP_9:         return LKEY_KP_9;
    case XK_KP_Separator:
    case XK_KP_Decimal:   return LKEY_KP_DECIMAL;
    case XK_KP_Equal:     return LKEY_KP_EQUAL;
    case XK_KP_Enter:     return LKEY_KP_ENTER;
    case XK_KP_Divide:    return LKEY_KP_DIVIDE;
    case XK_KP_Multiply:  return LKEY_KP_MULTIPLY;
    case XK_KP_Subtract:  return LKEY_KP_SUBTRACT;
    case XK_KP_Add:       return LKEY_KP_ADD;

    case XK_Shift_L:      return LKEY_LEFT_SHIFT;
    case XK_Shift_R:      return LKEY_RIGHT_SHIFT;

    case XK_Control_L:    return LKEY_LEFT_CONTROL;
    case XK_Control_R:    return LKEY_RIGHT_CONTROL;

    case XK_Alt_L:        return LKEY_LEFT_ALT;
    case XK_Alt_R:        return LKEY_RIGHT_ALT;

    case XK_Super_L:      return LKEY_LEFT_SUPER;
    case XK_Super_R:      return LKEY_RIGHT_SUPER;

    case XK_Menu:         return LKEY_MENU;
    default:
        break;
    }
}

bool x11event::keyPress(int key)
{
    return keyStateArray[key][0];
}

bool x11event::keyHit(int key)
{
    if(keyStateArray[key][0])
    {
        if(!keyStateArray[key][1])
        {
            keyStateArray[key][1] = true;
            return true;
        }else{
            return false;
        }
    }else{
        if(keyStateArray[key][1])
            keyStateArray[key][1] = false;
        return false;
    }

}

int x11event::translateButton(Display *d, int button)
{
    switch(button)
    {
    case Button1:        return LBUTTON_LEFT;
    case Button2:        return LBUTTON_CENTER;
    case Button3:        return LBUTTON_RIGHT;
    case Button4:        return LBUTTON_WHEEL_UP;
    case Button5:        return LBUTTON_WHEEL_DOWN;
        //    case Button6:        return LBUTTON_WHEEL_LEFT;
        //    case Button7:        return LBUTTON_WHEEL_RIGHT;
        //    case Button8:        return LBUTTON_8;
        //    case Button9:        return LBUTTON_9;
    default:             break;
    }
}

bool x11event::mousePress(int but)
{
    return mouseStateArray[but-1][0];
}

bool x11event::mouseHit(int but)
{

            if(mouseStateArray[but-1][0])
            {
                if(!mouseStateArray[but-1][1])
                {
                    mouseStateArray[but-1][1] = true;
                    return true;
                }else{
                    return false;
                }
            }else{
                if(mouseStateArray[but-1][1])
                    mouseStateArray[but-1][1] = false;
                return false;
            }
}

void x11event::update(Display *d, XEvent *event)
{
    switch (event->type) {
    case KeyPress:
        keyStateArray[translateKey(d, event->xkey.keycode)][0]=true;
        break;
    case KeyRelease:
        keyStateArray[translateKey(d, event->xkey.keycode)][0]=false;
        break;
    case ButtonPress:
        mouseStateArray[translateButton(d, event->xbutton.button)-1][0]=true;
        break;
    case ButtonRelease:
        mouseStateArray[translateButton(d, event->xbutton.button)-1][0]=false;
        break;
    default:
        break;
    }
}

