#ifndef X11WINDOW_H
#define X11WINDOW_H

#include "lwindow.h"
#include "log.h"
#include "x11event.h"
#include "x11platform.h"

#define GLX_CONTEXT_MAJOR_VERSION_ARB       0x2091
#define GLX_CONTEXT_MINOR_VERSION_ARB       0x2092


class x11window: public LWindow
{   
    int x, y;
    unsigned int width, height;
    int currentWidth, currentHeight;
    const char* title;
    unsigned int depth;
    bool fullscreen;
    bool vsync;
    bool doubleBuffered;

    Display *display;
    int screen;

    Window window;
    XSetWindowAttributes winAttr;


    XVisualInfo *vi;
    Colormap cmap;
    GLXContext context;


    Atom wmDelete;


    x11platform platform;
    x11event *ev;



public:
    x11window();
    ~x11window();
    void createWindow(int x, int y,unsigned int width,unsigned int height,unsigned int depth, bool fullscreen, bool vsync, bool doubleBuffered);
    void setWindowTitle(const char *title);
    void destroyWindow();
    void update();

    x11event *getEventHandler();

    void setFullscreen(bool mode);
    void setWindowFullscreen(bool mode);

    void setSwapInterval(int interval);

    int windowWidth();
    int windowHeight();

    void initGL();
    void resizeGL(unsigned int w, unsigned int h);
    void updateGL();
};

#endif // X11WINDOW_H
