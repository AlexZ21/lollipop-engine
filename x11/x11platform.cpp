#include "x11platform.h"

x11platform::x11platform()
{
    display = XOpenDisplay(0);
    screen = DefaultScreen(display);

    videoModes = NULL;

    oglMajor = 0;
    oglMinor = 0;

    glxExtensions = glXQueryExtensionsString(display, DefaultScreen(display));

    glXCreateContextAttribsARB = (glXCreateContextAttribsARBProc)getProcAddress("glXCreateContextAttribsARB");


    if(isExtensionSupported("GLX_MESA_swap_control"))
    {
        glXSwapIntervalMESA = (PFNGLXSWAPINTERVALMESAPROC)getProcAddress("glXSwapIntervalMESA");
        glXGetSwapIntervalMESA = (PFNGLXGETSWAPINTERVALMESAPROC)getProcAddress("glXGetSwapIntervalMESA");
        if(glXSwapIntervalMESA) Log::print()<<"Swap interval mesa is supported";
    }

    if(isExtensionSupported("GLX_SGI_swap_control"))
    {
        glXSwapIntervalSGI = (PFNGLXSWAPINTERVALSGIPROC)getProcAddress("glXSwapIntervalSGI");
        if(glXSwapIntervalSGI) Log::print()<<"Swap interval SGI is supported";
    }

    if(isExtensionSupported("GLX_EXT_swap_control"))
    {
        glXSwapIntervalEXT = (PFNGLXSWAPINTERVALEXTPROC)getProcAddress("glXSwapIntervalEXT");
        if(glXSwapIntervalEXT) Log::print()<<"Swap interval EXT is supported";
    }
}

x11platform::~x11platform()
{
    delete videoModes;
    delete glxExtensions;

    XFree(fbc);
    delete fbc;

    XFree(display);
    delete display;

}

int x11platform::getOpenGLVersion()
{
    glGetIntegerv(GL_MAJOR_VERSION, &oglMajor);
    glGetIntegerv(GL_MINOR_VERSION, &oglMinor);

    Log::print()<<"OpenGl version: "<<(int)oglMajor<<"."<<(int)oglMinor<<"  ";
}



void x11platform::getGLXVersion()
{
    glXQueryVersion(display, &glxMajor, &glxMinor);
    Log::print()<<"GLX version: "<<glxMajor<<"."<<glxMinor;
}



void x11platform::getBestVideoMode(int w, int h)
{
    XF86VidModeQueryVersion(display, &vmMajor, &vmMinor);
    Log::print()<<"XF86 video mode version: "<<vmMajor<<"."<<vmMinor;

    XF86VidModeGetAllModeLines(display, screen, &modeNum, &videoModes);
    Log::print()<<"XF86 mode number: "<<modeNum;

    desktopVideoMode = *videoModes[0];

    for (int i = 0; i < modeNum; i++)
    {
        Log::print()<<"Mode: "<<i;
        Log::print()<<"Resolution: "<<videoModes[i]->hdisplay<<"x"<<videoModes[i]->vdisplay;
        if ((videoModes[i]->hdisplay == w) && (videoModes[i]->vdisplay == h))
        {
            bestMode = i;
            Log::print()<<"Best mode("<<i<<"): "<<videoModes[i]->hdisplay<<"x"<<videoModes[i]->vdisplay;
        }
        Log::print();
    }

    bestVideoMode = *videoModes[bestMode];

    XFree(videoModes);
}


void x11platform::setVideoNode(XF86VidModeModeInfo *videomode)
{
    XF86VidModeLockModeSwitch(display, screen, 0);
    XF86VidModeSwitchToMode(display, screen, videomode);
    XF86VidModeSetViewPort(display, screen, 0, 0);
    XF86VidModeLockModeSwitch(display, screen, 1);
}


void x11platform::setFullscreenVideoMode()
{
    setVideoNode(&bestVideoMode);
}


void x11platform::setDesktopVideoMode()
{
    setVideoNode(&desktopVideoMode);
}



GLXFBConfig x11platform::getBestGLXFBConfig(const int *attr)
{
    GLXFBConfig *fbc = glXChooseFBConfig(display, DefaultScreen( display ), attr, &fbCount);

    if(fbc)
    {
        Log::print()<<"GLXFBConfig is found";
        Log::print()<<"Found "<<fbCount<<" matching FB configs.";

        int best_fbc = -1, worst_fbc = -1, best_num_samp = -1, worst_num_samp = 999;

        for (int i = 0; i < fbCount; i++)
        {
            XVisualInfo *vi = glXGetVisualFromFBConfig(display, fbc[i]);
            if (vi)
            {
                int samp_buf, samples;
                glXGetFBConfigAttrib(display, fbc[i], GLX_SAMPLE_BUFFERS, &samp_buf);
                glXGetFBConfigAttrib(display, fbc[i], GLX_SAMPLES       , &samples);

                Log::print()<<"Matching fbconfig "<<i<<", visual ID 0x"<<vi->visualid<<": SAMPLE_BUFFERS = "<<samp_buf<<", SAMPLES = "<<samples;

                if ( best_fbc < 0 || samp_buf && samples > best_num_samp )
                    best_fbc = i, best_num_samp = samples;
                if ( worst_fbc < 0 || !samp_buf || samples < worst_num_samp )
                    worst_fbc = i, worst_num_samp = samples;
            }
            XFree(vi);
        }
        GLXFBConfig bestFbc = fbc[best_fbc];
        XFree(fbc);
        return bestFbc;
    }else{
        return NULL;
    }
}



bool x11platform::isExtensionSupported(const char *extension)
{
    const char *start;
    const char *where, *terminator;

    where = strchr(extension, ' ');
    if ( where || *extension == '\0' )
        return false;

    for ( start = glxExtensions; ; ) {
        where = strstr( start, extension );

        if ( !where )
            break;

        terminator = where + strlen( extension );

        if ( where == start || *(where - 1) == ' ' )
            if ( *terminator == ' ' || *terminator == '\0' )
                return true;

        start = terminator;
    }

    return false;
}


void *x11platform::getProcAddress(const char *procname)
{
    return (void *)glXGetProcAddress((const GLubyte *)procname);
}



void x11platform::setSwapInterval(int interval)
{
    if(glXSwapIntervalSGI)
    {
        glXSwapIntervalSGI(interval);
        Log::print()<<"Set SGI swap interval: "<<interval;
    }else if(glXSwapIntervalMESA)
    {
        glXSwapIntervalMESA(interval);
        Log::print()<<"Set MESA swap interval: "<<interval;
    }
}
