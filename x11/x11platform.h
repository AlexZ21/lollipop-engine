#ifndef X11PLATFORM_H
#define X11PLATFORM_H
#include <X11/Xlib.h>
#include <X11/Xatom.h>
#include <X11/keysym.h>
#include <stdio.h>
#include <string.h>
#include <X11/extensions/xf86vmode.h>
#include <GL/gl.h>
#include <GL/glx.h>
#include <GL/glu.h>

#include "log.h"


typedef GLXContext (*glXCreateContextAttribsARBProc)(Display*, GLXFBConfig, GLXContext, Bool, const int*);
//typedef int (*PFNGLXSWAPINTERVALMESAPROC)(int);



class x11platform
{
public:
    x11platform();
    ~x11platform();

    //Display and screen
    Display *display;
    int screen;

    //Get OpenGl version
    GLint oglMajor, oglMinor;
    int getOpenGLVersion();

    //Get GLX vesion
    int glxMajor, glxMinor;
    void getGLXVersion();

    //Get videomodes
    XF86VidModeModeInfo desktopVideoMode;
    XF86VidModeModeInfo bestVideoMode;
    XF86VidModeModeInfo **videoModes;
    int vmMajor, vmMinor;
    int modeNum, bestMode;
    void getBestVideoMode(int w, int h);
    void setVideoNode(XF86VidModeModeInfo *videomode);
    void setFullscreenVideoMode();
    void setDesktopVideoMode();

    //Get best GLXFBConfig
    int fbCount;
    GLXFBConfig *fbc;
    GLXFBConfig getBestGLXFBConfig(const int *attr);

    //Extensions
    const char *glxExtensions;
    bool isExtensionSupported(const char *extension);

    //get proc adress
    void *getProcAddress(const char *procname);

    //Func for create context
    glXCreateContextAttribsARBProc glXCreateContextAttribsARB;

    //Swap controls
    PFNGLXSWAPINTERVALMESAPROC glXSwapIntervalMESA;
    PFNGLXGETSWAPINTERVALMESAPROC glXGetSwapIntervalMESA;

    PFNGLXSWAPINTERVALSGIPROC glXSwapIntervalSGI;

    PFNGLXSWAPINTERVALEXTPROC glXSwapIntervalEXT;

    void setSwapInterval(int interval);
};

#endif // X11PLATFORM_H
