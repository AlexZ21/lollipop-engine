#ifndef LSCENESIMPLECUBE_H
#define LSCENESIMPLECUBE_H

#include "lscenenode.h"
#include "lscenenodegroup.h"
#include "vector3d.h"

#include <GL/gl.h>
#include <GL/glx.h>
#include <GL/glu.h>

class LSceneNodeGroup;

class LSceneSimpleCube : public LSceneNode
{
    GLfloat rot;

    vector3d position;
    vector3d rotation;
    vector3d scale;

    LSceneNodeGroup *sceneNodeGroup;
public:
    LSceneSimpleCube(LSceneNodeGroup *sceneNodeGroup);

    void setPosition(vector3d position);
    void setRotation(vector3d rotation);
    void setScale(vector3d scale);

    void draw();
};

#endif // LSCENESIMPLECUBE_H
