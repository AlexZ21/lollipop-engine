#include "lscenenodegroup.h"

LSceneNodeGroup::LSceneNodeGroup()
{
    visible = true;
    state = true;
}

LSceneNode *LSceneNodeGroup::createSceneNode()
{
}

void LSceneNodeGroup::addSceneNode(LSceneNode *sceneNode)
{
    if(sceneNode)
    {
        Log::print()<<"Add scene node";
        sceneNodeList.push_back(sceneNode);
    }else{
        Log::print()<<"Erorr add scene node";
    }
}

void LSceneNodeGroup::removeSceneNode(LSceneNode *sceneNode)
{
    if(sceneNode)
    {
        Log::print()<<"remove scene node";
        sceneNodeList.remove(sceneNode);
    }else{
        Log::print()<<"Error remove scene node";
    }
}

void LSceneNodeGroup::renoveAllSceneNode()
{
    if(!sceneNodeList.empty())
    {
        sceneNodeList.clear();
        Log::print()<<"Remove all scene node";
    }else{
        Log::print()<<"Scene node list is empty";
    }
}

void LSceneNodeGroup::setActive(bool state)
{
    this->state = state;
}

bool LSceneNodeGroup::isActive()
{
    if(this->state)
        return true;
    else
        return false;
}

void LSceneNodeGroup::setVisible(bool visible)
{
    this->visible = visible;
}

bool LSceneNodeGroup::isVisible()
{
    if(this->visible)
        return true;
    else
        return false;
}

void LSceneNodeGroup::draw()
{
    if(this->visible)
    {
        if(!sceneNodeList.empty())
        {
            for(std::list<LSceneNode*>::iterator it = sceneNodeList.begin(); it != sceneNodeList.end(); ++it)
            {
                (*it)->draw();
            }
        }
    }
}
