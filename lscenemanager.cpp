#include "lscenemanager.h"

LSceneManager::LSceneManager()
{
}

LScene *LSceneManager::createScene()
{
    LScene *newScene = new LScene();
    if(newScene)
    {
        sceneList.push_back(newScene);
        Log::print()<<"Create new scene";
        return newScene;
    }else{
        Log::print()<<"Error create new scene";
        return 0;
    }
}

void LSceneManager::addScene(LScene *scene)
{
    if(scene)
    {
        Log::print()<<"Add scene";
        sceneList.push_back(scene);
    }else{
        Log::print()<<"Erorr add scene";
    }
}

void LSceneManager::removeScene(LScene *scene)
{
    if(scene)
    {
        sceneList.remove(scene);
        Log::print()<<"Remove scene";
    }else{
        Log::print()<<"Error remove scene";
    }
}

void LSceneManager::removeAllScene()
{
    if(!sceneList.empty())
    {
        sceneList.clear();
        Log::print()<<"Remove all scene";
    }else{
        Log::print()<<"Scene list is empty";
    }
}

void LSceneManager::update()
{
    if(!sceneList.empty())
    {
        for(std::list<LScene*>::iterator it = sceneList.begin(); it != sceneList.end(); ++it)
        {
            (*it)->draw();
        }
    }
}
