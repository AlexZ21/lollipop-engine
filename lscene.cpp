#include "lscene.h"

LScene::LScene()
{
    visible = true;
    state = true;

    guiManager = new LGUIManager();
    sceneCamera = NULL;

    rootSceneNodeGroup = new LSceneNodeGroup();
    sceneNodeGroupList.push_back(rootSceneNodeGroup);

}

LScene::~LScene()
{
    delete guiManager;
    delete sceneCamera;
    delete rootSceneNodeGroup;
}



//**********************************************
//Methods for scene node group
//**********************************************
LSceneNodeGroup *LScene::createSceneNodeGroup()
{
    LSceneNodeGroup *sceneNodeGroup = NULL;
    sceneNodeGroup = new LSceneNodeGroup();
    if(sceneNodeGroup)
    {
        sceneNodeGroupList.push_back(sceneNodeGroup);
        Log::print()<<"Create scene node group";
    }else{
        Log::print()<<"Error create scene node group";
        return NULL;
    }
}

void LScene::addSceneNodeGroup(LSceneNodeGroup *sceneNodeGroup)
{
    if(sceneNodeGroup)
    {
        Log::print()<<"Add scene node group";
        sceneNodeGroupList.push_back(sceneNodeGroup);
    }else{
        Log::print()<<"Erorr add scene node group";
    }
}

void LScene::removeSceneNodeGroup(LSceneNodeGroup *sceneNodeGroup)
{
    if(sceneNodeGroup)
    {
        Log::print()<<"Remove scene node group";
        sceneNodeGroupList.remove(sceneNodeGroup);
    }else{
        Log::print()<<"Erorr remove scene node group";
    }
}

LSceneNodeGroup *LScene::getRootSceneNodeGroup()
{
    if(rootSceneNodeGroup)
        return rootSceneNodeGroup;
    else
        return NULL;
}



//************************************************
//Methods for add/remove scene node in root group
//************************************************
LSceneNode *LScene::createSceneNode()
{

}

void LScene::addSceneNode(LSceneNode *sceneNode)
{
    if(sceneNode && rootSceneNodeGroup)
    {
        Log::print()<<"Add scene node";
        rootSceneNodeGroup->addSceneNode(sceneNode);
    }else{
        Log::print()<<"Erorr add scene node";
    }
}

void LScene::removeSceneNode(LSceneNode *sceneNode)
{
    if(sceneNode)
    {
        Log::print()<<"remove scene node";
        rootSceneNodeGroup->removeSceneNode(sceneNode);
    }else{
        Log::print()<<"Error remove scene node";
    }
}

void LScene::renoveAllSceneNode()
{
    rootSceneNodeGroup->renoveAllSceneNode();
}



//************************************************
//Methods for add/remove gui in scene
//************************************************
LGUI *LScene::createGUI()
{
    return guiManager->createGui();
}

void LScene::addGUI(LGUI *gui)
{
    guiManager->addGui(gui);
}

void LScene::removeGUI(LGUI *gui)
{
    guiManager->removeGui(gui);
}

void LScene::renoveAllGUI()
{
    guiManager->renoveAllGui();
}

LSceneCamera *LScene::createSceneCamera()
{
    if(!sceneCamera)
    {
        sceneCamera = new LSceneCamera();
        Log::print()<<"Create scene camera";
        return sceneCamera;
    }else{
        Log::print()<<"Error create camera";
        return NULL;
    }
}



//************************************************
//Methods for control state of scene
//************************************************
void LScene::setActive(bool state)
{
    this->state = state;
}

bool LScene::isActive()
{
    if(this->state)
        return true;
    else
        return false;
}

void LScene::setVisible(bool visible)
{
    this->visible = visible;
}

bool LScene::isVisible()
{
    if(this->visible)
        return true;
    else
        return false;
}



//************************************************
//Methods for draw scene
//************************************************
void LScene::draw()
{
    if(this->visible)
    {
        if(sceneCamera) sceneCamera->update();

        if(!sceneNodeGroupList.empty())
        {
            for(std::list<LSceneNodeGroup*>::iterator it = sceneNodeGroupList.begin(); it != sceneNodeGroupList.end(); ++it)
            {
                (*it)->draw();
            }
        }
    }
}
