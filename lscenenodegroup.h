#ifndef LSCENENODEGROUP_H
#define LSCENENODEGROUP_H

#include "lscenenode.h"
#include "log.h"

#include <list>


class LSceneNodeGroup
{
    bool state;
    bool visible;
    std::list<LSceneNode *> sceneNodeList;
public:
    LSceneNodeGroup();

    LSceneNode *createSceneNode();
    void addSceneNode(LSceneNode *sceneNode);
    void removeSceneNode(LSceneNode *sceneNode);
    void renoveAllSceneNode();

    void setActive(bool state);
    bool isActive();

    void setVisible(bool visible);
    bool isVisible();

    void draw();
};

#endif // LSCENENODEGROUP_H
