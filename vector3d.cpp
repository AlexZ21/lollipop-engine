#include "vector3d.h"

vector3d::vector3d()
{
    x = 0;
    y = 0;
    z = 0;
}

vector3d::vector3d(float x, float y, float z)
{
    this->x = x;
    this->y = y;
    this->z = z;
}

vector3d::vector3d(const vector3d &vec)
{
    this->x = vec.x;
    this->y = vec.y;
    this->z = vec.z;
}

vector3d &vector3d::operator =(const vector3d &vec)
{
    this->x = vec.x;
    this->y = vec.y;
    this->z = vec.z;
    return *this;
}

void vector3d::set(float x, float y, float z)
{
    this->x = x;
    this->y = y;
    this->z = z;
}
