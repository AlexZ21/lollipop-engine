#ifndef LWINDOW_H
#define LWINDOW_H

#include "log.h"
#include "levent.h"

class LWindow
{

public:
    LWindow();
    virtual void createWindow(int x, int y,unsigned int width,unsigned int height,unsigned int depth, bool fullscreen, bool vsync, bool doubleBuffered) = 0;
    virtual void setWindowTitle(const char * title)=0;
    virtual void destroyWindow() = 0;
    virtual void update() = 0;

    virtual LEvent *getEventHandler() = 0;

    virtual void setFullscreen(bool mode) = 0;

    virtual void setSwapInterval(int interval) = 0;

    virtual int windowWidth() = 0;
    virtual int windowHeight() = 0;

    virtual void initGL() = 0;
    virtual void resizeGL(unsigned int w, unsigned int h) = 0;
    virtual void updateGL() = 0;
};

#endif // LWINDOW_H
