#include "ldevice.h"

LollipopDevice::LollipopDevice()
{
    window = NULL;
    sceneManager = NULL;

}

LollipopDevice::~LollipopDevice()
{
    window->destroyWindow();
    delete window;
    delete sceneManager;
}

LWindow * LollipopDevice::createWindow(unsigned int width, unsigned int height, unsigned int depth, bool fullscreen, bool vsync, bool doubleBuffered)
{
    if(window == NULL)
    {
        window = new x11window();
        window->createWindow(0,0,width,height, depth, fullscreen, vsync, doubleBuffered);
    }
    return window;
}

LSceneManager *LollipopDevice::getSceneManager()
{
    if(sceneManager == NULL)
    {
        sceneManager = new LSceneManager();
    }
    return sceneManager;
}

void LollipopDevice::update()
{

    glMatrixMode(GL_PROJECTION);
    glLoadIdentity();
    gluPerspective(45.0f, (GLfloat)window->windowWidth() / (GLfloat)window->windowHeight(), 0.1f, 100.0f);
    glMatrixMode(GL_MODELVIEW);

    glClear(GL_COLOR_BUFFER_BIT | GL_DEPTH_BUFFER_BIT);

    if(sceneManager) sceneManager->update();

    window->update();
}

void LollipopDevice::exit()
{
    window->destroyWindow();
}


