#include "lscenesimplecube.h"

LSceneSimpleCube::LSceneSimpleCube(LSceneNodeGroup *sceneNodeGroup)
{
    this->sceneNodeGroup = NULL;
    this->sceneNodeGroup = sceneNodeGroup;
    if(this->sceneNodeGroup)
        this->sceneNodeGroup->addSceneNode(this);
    rot = 0;
    scale.set(1,1,1);
}

void LSceneSimpleCube::setPosition(vector3d position)
{
        this->position = position;
}

void LSceneSimpleCube::setRotation(vector3d rotation)
{
        this->rotation = rotation;
}

void LSceneSimpleCube::setScale(vector3d scale)
{
        this->scale = scale;
}

void LSceneSimpleCube::draw()
{

    glPushMatrix();
    //glLoadIdentity();

    glTranslatef(position.x, position.y, position.z);

    glRotatef(rotation.z, 0.0, 0.0, 1.0);
    glRotatef(rotation.y, 0.0, 1.0, 0.0);
    glRotatef(rotation.x, 1.0, 0.0, 0.0);

    glScalef(scale.x, scale.y, scale.z);


    glBegin(GL_QUADS);
    /* top of cube */
    glColor3f(0.0f, 1.0f, 0.0f);
    glVertex3f(1.0f, 1.0f, -1.0f);
    glVertex3f(-1.0f, 1.0f, -1.0f);
    glVertex3f(-1.0f, 1.0f, 1.0f);
    glVertex3f(1.0f, 1.0f, 1.0f);
    /* bottom of cube */
    glColor3f(1.0f, 0.5f, 0.0f);
    glVertex3f(1.0f, -1.0f, 1.0f);
    glVertex3f(-1.0f, -1.0f, 1.0f);
    glVertex3f(-1.0f, -1.0f, -1.0f);
    glVertex3f(1.0f, -1.0f, -1.0f);
    /* front of cube */
    glColor3f(1.0f, 0.0f, 0.0f);
    glVertex3f(1.0f, 1.0f, 1.0f);
    glVertex3f(-1.0f, 1.0f, 1.0f);
    glVertex3f(-1.0f, -1.0f, 1.0f);
    glVertex3f(1.0f, -1.0f, 1.0f);
    /* back of cube */
    glColor3f(1.0f, 1.0f, 0.0f);
    glVertex3f(-1.0f, 1.0f, -1.0f);
    glVertex3f(1.0f, 1.0f, -1.0f);
    glVertex3f(1.0f, -1.0f, -1.0f);
    glVertex3f(-1.0f, -1.0f, -1.0f);
    /* right side of cube */
    glColor3f(1.0f, 0.0f, 1.0f);
    glVertex3f(1.0f, 1.0f, -1.0f);
    glVertex3f(1.0f, 1.0f, 1.0f);
    glVertex3f(1.0f, -1.0f, 1.0f);
    glVertex3f(1.0f, -1.0f, -1.0f);
    /* left side of cube */
    glColor3f(0.0f, 1.0f, 1.0f);
    glVertex3f(-1.0f, 1.0f, 1.0f);
    glVertex3f(-1.0f, 1.0f, -1.0f);
    glVertex3f(-1.0f, -1.0f, -1.0f);
    glVertex3f(-1.0f, -1.0f, 1.0f);
    glEnd();
    glPopMatrix();
}
