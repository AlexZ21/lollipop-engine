#ifndef LOG_H
#define LOG_H

#include <string>
#include <iostream>
#include <fstream>
#include <time.h>

class Log
{
public:
    static void startLog(const char *name, bool toConsole);
    static Log &print();
    static void closeLog();

    template <class T>
    Log& operator<<(T a)
    {
        if(newLog.enable)
            std::cout<<a;
        std::cout.flush();

        if(newLog.fileStream.is_open())
            newLog.fileStream<<a;
        newLog.fileStream.flush();
    }

protected:
    static Log newLog;
private:
    const char *nameFile = NULL;
    std::ofstream fileStream;
    time_t t;
    tm* aTm;
    bool enable;
    Log();
    ~Log();


};


#endif // LOG_H
